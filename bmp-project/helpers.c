#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "helpers.h"

// Flips the image
void flip(int height, int width, RGBTRIPLE image[height][width])
{
    RGBTRIPLE pixel;

    // Half the height because we want to change the places of the first and second half
    for (int i = 0; i < height / 2; i++)
    {
        for (int j = 0; j < width; j++)
        {
            // changes the values
            pixel = image[i][j];
            image[i][j] = image[height - i - 1][j];
            image[height - i - 1][j] = pixel;
        }
    }
}

// Blur the image
// Box Blur - https://en.wikipedia.org/wiki/Box_blur
void blur(int height, int width, RGBTRIPLE image[height][width])
{
    RGBTRIPLE copy[height][width];

    // iterate through each pixel in the image
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            int sumRed = 0, sumGreen = 0, sumBlue = 0;
            int count = 0;

            // iterate through each neighbor pixel
            for (int k = -1; k <= 1; k++)
            {
                for (int l = -1; l <= 1; l++)
                {
                    // used for the pixels at the edge
                    int new_i = i + k;
                    int new_j = j + l;

                    // check if the neighbor pixel is within bounds of the image
                    if (new_i >= 0 && new_i < height && new_j >= 0 && new_j < width)
                    {
                        sumRed += image[new_i][new_j].rgbtRed;
                        sumGreen += image[new_i][new_j].rgbtGreen;
                        sumBlue += image[new_i][new_j].rgbtBlue;
                        count++;
                    }
                }
            }

            // set the pixel value in the copy image
            copy[i][j].rgbtRed = round((float) sumRed / count);
            copy[i][j].rgbtGreen = round((float) sumGreen / count);
            copy[i][j].rgbtBlue = round((float) sumBlue / count);
        }
    }

    // update the original image with the blurred pixels from the copy image
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            // the images takes the value of copy
            image[i][j] = copy[i][j];
        }
    }

}

// Crop image
RGBTRIPLE** crop(int height, int width, RGBTRIPLE image[height][width], int x, int y, int h, int w)
{
    RGBTRIPLE** cropped_image = malloc(h * sizeof(RGBTRIPLE*));
    for (int i = 0; i < h; i++) {
        cropped_image[i] = malloc(w * sizeof(RGBTRIPLE));
    }

    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < w; j++)
        {
            cropped_image[i][j] = image[y + i][x + j];
        }
    }

    return cropped_image;
}

// Asks the user to specify the x, y, height and the width. Checks if it is integer and in the proper limit.
void input_user(int height, int width, int* x, int* y, int* h, int* w)
{
    printf("Enter the x coordinate of the top-left corner: ");
    while (scanf("%d", x) != 1 || *x < 0 || *x >= width)
    {
        printf("Invalid input. Please enter a valid integer between 0 and %d.\n", width - 1);
        while (getchar() != '\n');
        printf("Enter the x coordinate of the top-left corner: ");
    }

    printf("Enter the y coordinate of the top-left corner: ");
    while (scanf("%d", y) != 1 || *y < 0 || *y >= height)
    {
        printf("Invalid input. Please enter a valid integer between 0 and %d.\n", height - 1);
        while (getchar() != '\n');
        printf("Enter the y coordinate of the top-left corner: ");
    }

    printf("Enter the height of the crop: ");
    while (scanf("%d", h) != 1 || *h < 1 || *y + *h > height)
    {
        printf("Invalid input. Please enter a valid integer greater than 0 and less than or equal to %d.\n", height - *y);
        while (getchar() != '\n');
        printf("Enter the height of the crop: ");
    }

    printf("Enter the width of the crop: ");
    while (scanf("%d", w) != 1 || *w < 1 || *x + *w > width)
    {
        printf("Invalid input. Please enter a valid integer greater than 0 and less than or equal to %d.\n", width - *x);
        while (getchar() != '\n');
        printf("Enter the width of the crop: ");
    }
}

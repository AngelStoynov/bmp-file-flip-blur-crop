#include "bmp.h"

// Blur image
void blur(int height, int width, RGBTRIPLE image[height][width]);

// Flips the image vertically
void flip(int height, int width, RGBTRIPLE image[height][width]);

// Crop image
RGBTRIPLE** crop(int height, int width, RGBTRIPLE image[height][width], int x, int y, int h, int w);

// User input
void input_user(int height, int width, int* x, int* y, int* h, int* w);
